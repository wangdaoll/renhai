package com.rh.zuul.filterpre;

import com.alibaba.fastjson.JSON;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.rh.zuul.filterorder.FilterOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import renhai.util.CodeUtil;
import renhai.util.ResponseContext;
import renhai.util.RestTemplateUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;

/**
 * 登陆过滤器
 *
 * @author 57556
 */
@Component
public class loginFilter extends ZuulFilter {
    private static final String HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR";
    @Autowired
    private RestTemplateUtil restTemplateUtil;

    /**
     * 过滤器类型
     *
     * @return
     */
    @Override
    public String filterType() {//前置过滤器
        return FilterConstants.PRE_TYPE;
    }

    /**
     * 顺序
     *
     * @return
     */
    @Override
    public int filterOrder() {
        return FilterOrder.FILTER_LOGIN;
    }

    /**
     * 过滤器是否生效
     *
     * @return
     */
    @Override
    public boolean shouldFilter() {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        //注册和登录接口不拦截，其他接口都要拦截校验 token
        if (FilterOrder.LOGINFILTER_URL.get(request.getRequestURI()) != null || FilterOrder.OVERLOOK_URL.get(request.getRequestURI())!=null) {
            return false;
        }
        return true;
    }

    /**
     * 业务逻辑
     *
     * @return
     * @throws ZuulException
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = getCurrentContext();
        ResponseContext rc = restTemplateUtil.checkToken();
        HttpServletResponse response = requestContext.getResponse();
        HttpServletRequest request = requestContext.getRequest();
        String remoteAddr = request.getRemoteAddr();
        requestContext.getZuulRequestHeaders().put(HTTP_X_FORWARDED_FOR, remoteAddr);
        if (rc == null) {
            //  response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE, PATCH");
            requestContext.setSendZuulResponse(false);
            //返回状态码
            requestContext.setResponseStatusCode(401);
            //返回错误信息
            ResponseContext responseContext = new ResponseContext<>("未登录", 0, CodeUtil.NOT_LOG);
            response.setHeader("errorMsg", JSON.toJSONString(responseContext));
            return null;
        } else {
            if (rc.getState() == 0) {
                requestContext.setSendZuulResponse(false);
                //返回状态码
                requestContext.setResponseStatusCode(401);
                //返回错误信息
                response.setHeader("errorMsg", JSON.toJSONString(rc));
            }
        }
        return null;
    }
}
