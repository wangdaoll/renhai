package com.rh.log.service;

import com.rh.entity.log.RenHaiLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangdao
 * @since 2020-04-21
 */
public interface RenHaiLogService extends IService<RenHaiLog> {

   PageInfo<RenHaiLog> list(Page page);

}
