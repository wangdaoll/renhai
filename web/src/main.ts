import Vue from "vue";
import Vuex from "vuex";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import util from "./common/util";
import "default-passive-events";
// @ts-ignore
import uploader from "vue-simple-uploader";
// @ts-ignore
/*import VideoPlayer from "vue-video-player";
import "vue-video-player/src/custom-theme.css";
import "video.js/dist/video-js.css";

Vue.use(VideoPlayer);*/
Vue.use(ElementUI, { size: 'medium'})
Vue.use(ElementUI);
Vue.use(Vuex);
Vue.config.productionTip = false;
Vue.prototype.$util = util;
Vue.prototype.$store = store;
/**
 * 文件上传
 */
Vue.use(uploader);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
