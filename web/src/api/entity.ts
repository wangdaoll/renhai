import * as request from "@/axios/axios";
import store from "@/store";

let _ = require("lodash");

export function entitySaveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiEntity/saveUpdate",
    param
  );
}

export function entityInitAllBeans() {
  return request.post(
      store.state.url.user + "/api/users/renHaiEntity/initAllBeans",
  );
}

export function entityList(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiEntity/getList",
    param
  );
}

export function filedSaveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiField/saveUpdate",
    param
  );
}

export function filedDelete(param: {}) {
  return request.del(
      store.state.url.user + "/api/users/renHaiField/del",
      param
  );
}

export function filedList(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiField/getList",
    param
  );
}
export function filedConfigSaveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiFieldConfig/saveUpdate",
    param
  );
}
export function filedConfigList(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/renHaiFieldConfig/getList",
    param
  );
}
export function filedConfigChildrenList(param: {}) {
  return request.post(
      store.state.url.user + "/api/users/renHaiFieldConfig/getTreeDate",
      param
  );
}
