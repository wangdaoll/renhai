import * as request from "@/axios/axios";
import store from "@/store";

export function saveUpdate(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/user/saveUpdate",
    param
  );
}

export function del(param: {}) {
  return request.del(store.state.url.user + "/api/users/user/delete", param);
}

export function details(param: {}) {
  return request.post(
    store.state.url.user + "/api/users/user/getDetails",
    param
  );
}

/**
 * 检查用户名是否重复
 * @param param
 */
export async function checkUserName(param: {}) {
  return await request.post(
    store.state.url.user + "/api/users/user/getUserNameCount",
    param
  );
}
