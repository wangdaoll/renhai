const webpack = require("webpack");
module.exports = {
  devServer: {
    port: 9806, //端口号
    open: true, //启动打开浏览器
    disableHostCheck: true,
    //配置跨区问题
    proxy: {
      "/api": {
        target: "http://127.0.0.1:8910/renHai", //API服务器的地址
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      }
    }
  },
  configureWebpack: {
    //https://www.cnblogs.com/ricolee/p/vue-debugging.html
    devtool: 'source-map',//默认是：cheap-module-eval-source-map
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "windows.jQuery": "jquery"
      })
    ]
  }
};
