package renhai.util;

public class CodeUtil {
    /**
     * 未登录
     */
    public static final Integer NOT_LOG = 600;
    /**
     * 登陆过期
     */
    public static final Integer PAST_DU_LOG = 601;
}
