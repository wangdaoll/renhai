package renhai.util.error;

/**
 * @Author: wangdao
 * @Date: 2021/2/19 16:53
 */
public class RenhaiException extends BasicException {
    public RenhaiException(String message) {
        super(0, message);
    }
}
