package renhai.util.bean;

import javax.swing.table.TableColumn;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** Field 操作
 * @author 57556
 */
public class FieldUtil {
    /**
     * 获取所有字段
     * @param clazz
     * @return
     */
    public static List<Field> getAllField(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        while (clazz != null) {
            fields.addAll(new ArrayList<>(Arrays.asList(clazz.getDeclaredFields())));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

    /** 字段类型;
     1-字符串
     2-数字
     3-小数
     4-long
     5-文本
     6-大文本 */
    public static Integer getType(Class<?> clazz) {
        if(clazz == String.class){
              return 1;
        }
        if(clazz == Integer.class){
            return 2;
        }
        if(clazz == Double.class){
            return 3;
        }
        if(clazz == Long.class){
            return 4;
        }
        return 0;
    }








}
