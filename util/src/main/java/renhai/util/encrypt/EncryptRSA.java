package renhai.util.encrypt;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


public class EncryptRSA {

    private static String src = "my name is cuichongdong";

    private static String privateKey;
    private static String publicKey;

    public EncryptRSA() {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        } catch (Exception e) {
            System.out.println("11111");
        }
        if (keyPairGenerator != null) {
            keyPairGenerator.initialize(512);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            RSAPublicKey rSAPublicKey = (RSAPublicKey) keyPair.getPublic();
            RSAPrivateKey private1 = (RSAPrivateKey) keyPair.getPrivate();
            publicKey = Base64.encodeBase64String(rSAPublicKey.getEncoded());
            privateKey = Base64.encodeBase64String(private1.getEncoded());
            System.out.println("公钥:" + publicKey);
            System.out.println("私钥:" + privateKey);
        } else {
            publicKey = "";
            privateKey = "";
        }
    }

    /**
     * 私钥加密
     *
     * @param str
     * @return
     */
    public static String getPrivateEncryptRSA(String str) {
        try {
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
            KeyFactory instance = KeyFactory.getInstance("RSA");
            KeyFactory keyFactory = instance;
            PrivateKey privateKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            byte[] result = cipher.doFinal(src.getBytes());
            return Base64.encodeBase64String(result);
        } catch (Exception e) {

        }
        return "";
    }

    /**
     * 公钥解密
     *
     * @param str
     * @return
     */
    public static String getPublicDecodeRAS(String str) {
        try {
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKey));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] result = cipher.doFinal(src.getBytes());
            return Base64.encodeBase64String(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    /**
     * 公钥加密
     *
     * @param str
     * @return
     */
    public static String getPublicEncryptRSA(String str) {
        try {
            PKCS8EncodedKeySpec pKCS8ENCODEDKEYSPEC = new PKCS8EncodedKeySpec(Base64.decodeBase64(publicKey));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(pKCS8ENCODEDKEYSPEC);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            byte[] result = cipher.doFinal(str.getBytes());
            return new String(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    /**
     * 私钥解密
     *
     * @param str
     * @return
     */
    public static String getPrivateDecodeRAS(String str) {
        try {
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(publicKey));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] result = cipher.doFinal(Base64.decodeBase64(str));
            return new String(result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "";
    }

    public static void main(String[] args) {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        } catch (Exception e) {
            System.out.println("11111");
        }
        if (keyPairGenerator != null) {
            keyPairGenerator.initialize(512);
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            RSAPublicKey rSAPublicKey = (RSAPublicKey) keyPair.getPublic();
            RSAPrivateKey private1 = (RSAPrivateKey) keyPair.getPrivate();
            publicKey = Base64.encodeBase64String(rSAPublicKey.getEncoded());
            privateKey = Base64.encodeBase64String(private1.getEncoded());
            // System.out.println("公钥:"+publicKey);
            System.out.println("私钥:" + privateKey);
        }
    }
}
