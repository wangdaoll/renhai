package renhai.util;

import renhai.util.error.BusinessException;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 57556
 */
public class Constant {

    public static final Map<Integer,String> COMMON_TYPE = new HashMap<Integer, String>(2){
        {
            put(1,"目录");
        }
    };

    public static final Map<Integer,String> TEST_TYPE = new HashMap<Integer, String>(2){
        {
            put(1,"类型-1");
            put(2,"类型-2");
        }
    };
    public static final Map<Integer,String> FIELD_CONFIG_TYPE = new HashMap<Integer, String>(2){
        {
            put(1,"boolean");
            put(2,"Integer");
            put(3,"String");
            put(4,"node");
            put(5,"");
        }
    };


    public static final ResponseContext<String> ERROR = new ResponseContext<>(0, "服务错误请重试");

    public static final ResponseContext<String> SAVE_S = new ResponseContext<>(1, "添加成功");

    public static final ResponseContext<String> UPDATE_S = new ResponseContext<>(1, "修改成功");

    public static final ResponseContext<String> DELETE_S = new ResponseContext<>(1, "删除成功");

    public static final ResponseContext<String> SAVE_E = new ResponseContext<>(1, "添加失败");

    public static final ResponseContext<String> UPDATE_E = new ResponseContext<>(1, "修改失败");

    public static final ResponseContext<String> DELETE_E = new ResponseContext<>(1, "删除失败");

    public static BusinessException getBusinessException(String mes) {
        return new BusinessException(2, mes);
    }
}
