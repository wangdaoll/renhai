package com.rh.entity.users;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rh.entity.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import renhai.util.json.LongJsonDeserializer;
import renhai.util.json.LongJsonSerializer;

/**
 * <p>
 *  
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value="RenHaiField对象", description=" ")
public class RenHaiField extends BaseBean {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字段显示名称")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "字段名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "字段长度")
    @TableField("length")
    private Integer length;

    @ApiModelProperty(value = "关联实体")
    @TableField("entity_id")
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @JsonSerialize(using = LongJsonSerializer.class)
    private Long entityId;

    /** 字段类型;
     1-字符串
     2-数字
     3-小数
     4-long
     5-文本
     6-大文本 */
    @ApiModelProperty(value = "字段类型")
    @TableField("type")
    private Integer type;

    @ApiModelProperty(value = "字段关联实体")
    @TableField("field_entity_id")
    private Long fieldEntityId ;
    /** 关系;
     1-一对一
     2-多对一
     3-多对多 */
    @ApiModelProperty(value = "关系")
    @TableField("relation")
    private Integer relation ;




}
