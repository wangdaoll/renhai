package com.rh.entity.users;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 公共组件 
 * </p>
 *
 * @author wangdao
 * @since 2020-07-22
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RenHaiCommonModule对象", description="公共组件 ")
public class RenHaiCommonModule implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;

    @ApiModelProperty(value = "创建人id")
    @TableField("created_id")
    private String createdId;

    @ApiModelProperty(value = "创建人姓名")
    @TableField("create_name")
    private String createName;

    @ApiModelProperty(value = "创建时间")
    @TableField("created_time")
    private Date createdTime;

    @ApiModelProperty(value = "更新人")
    @TableField("updated_id")
    private String updatedId;

    @ApiModelProperty(value = "更新人姓名")
    @TableField("updated_name")
    private String updatedName;

    @ApiModelProperty(value = "更新时间")
    @TableField("updated_time")
    private Date updatedTime;

    @ApiModelProperty(value = "公共组件名称 公共组件名称")
    @TableField("common_module_name")
    private String commonModuleName;

    @ApiModelProperty(value = "公共组件code 公共组件code")
    @TableField("common_module_code")
    private String commonModuleCode;

    @ApiModelProperty(value = "公共组件说明 公共组件说明")
    @TableField("common_module_explain")
    private String commonModuleExplain;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "公共组件父级id 公共组件父级id")
    @TableField("common_module_pid")
    private String commonModulePid;

    @ApiModelProperty(value = "类型")
    @TableField("common_module_type")
    private Integer commonModuleType;


}
