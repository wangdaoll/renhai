package com.rh.entity.user;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangdao
 * @since 2020-04-29
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RenHaiUserFile对象", description="")
public class RenHaiUserFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户文件id")
    @TableField("user_file_id")
    private String userFileId;

    @ApiModelProperty(value = "文件名称")
    @TableField("file_name")
    private String fileName;

    @ApiModelProperty(value = "文件地址")
    @TableField("file_url")
    private String fileUrl;

    @ApiModelProperty(value = "业务id")
    @TableField("business_id")
    private String businessId;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("status")
    private Integer status;

    @TableField("create_user_id")
    private String createUserId;


}
