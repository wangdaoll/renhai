package com.rh.entity.role;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@Getter
@Setter
@ApiModel("角色")
public class RenHaiRole implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModelProperty(name = "roleId", value = "角色id")
    @TableId
    private String roleId;
    /**
     * 角色名称
     */
    @ApiModelProperty(name = "roleName", value = "角色名称", required = true)
    private String roleName;
    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private Integer status;
    /**
     * 角色类型
     */
    @ApiModelProperty(name = "roleType", value = "角色类型")
    private String roleType;

    @ApiModelProperty(name = "createTime", value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    private Date createTime;
    @ApiModelProperty(name = "updateTime", value = "修改时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:sss")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:sss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(name = "moduleIds", value = "模块id")
    @TableField(exist = false)
    private String[] moduleIds;
    @ApiModelProperty(name = "renHaiRoleModuleList", value = "模块")
    @TableField(exist = false)
    private List<RenHaiRoleModule> renHaiRoleModuleList;

    public String[] getModuleIds() {
        if (renHaiRoleModuleList != null && !renHaiRoleModuleList.isEmpty()) {
            String[] moduleIds = new String[renHaiRoleModuleList.size()];
            for (int i = 0; i < moduleIds.length; i++) {
                moduleIds[i] = renHaiRoleModuleList.get(i).getRenHaiModule();
            }
            return moduleIds;
        } else {
            return this.moduleIds;
        }
    }

    @Override
    public String toString() {
        return "RenHaiRole{" +
                ", roleId=" + roleId +
                ", roleName=" + roleName +
                ", status=" + status +
                ", roleType=" + roleType +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                "}";
    }
}
