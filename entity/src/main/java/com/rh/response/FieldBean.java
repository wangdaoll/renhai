package com.rh.response;

import com.rh.response.form.FormParamBean;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author: wangdao
 * @Date: 2021/1/24 17:56
 */
@Getter
@Setter
public class FieldBean {

    private String name;

    private boolean key;

    private String title;

    private FormParamBean formParam = new FormParamBean();


}
