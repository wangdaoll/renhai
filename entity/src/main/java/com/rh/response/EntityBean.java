package com.rh.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wangdao
 * @Date: 2021/1/24 15:35
 */

@Getter
@Setter
public class EntityBean {
    /**
     * 实体名称
     */
    private String name;
    /**
     * 实体标题
      */
    private String title;
    /**
     * 表格参数
     */
    private TableBean table = new TableBean();
    /**
     * 字段 表格显示列
     */
    private List<FieldBean> fields = new ArrayList<>();

}
