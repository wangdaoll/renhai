package com.rh.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: wangdao
 * @Date: 2021/1/24 18:03
 */
@Getter
@Setter
public class PropBean {
    private boolean required;
    private String type;
    private String message;
}
