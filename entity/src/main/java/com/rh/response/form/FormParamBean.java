package com.rh.response.form;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: wangdao
 * @Date: 2021/1/24 17:59
 */
@Getter
@Setter
public class FormParamBean {
    /**
     * 是否多选
     */
    private boolean multi;

    private ParamsBean params = new ParamsBean();

}
