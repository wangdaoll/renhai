package com.rh.response.form;

import com.rh.response.PropBean;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wangdao
 * @Date: 2021/1/24 18:00
 */
@Getter
@Setter
public class ParamsBean {

    private String el;

    private String label;

    private String model;

    private boolean multiple;
    /**
     * 多选配置参数
     */
    private CandidateBean candidate = new CandidateBean();
    /**
     * 字段类型;
     * 1-字符串
     * 2-数字
     * 3-小数
     * 4-long
     * 5-文本
     * 6-大文本
     */
    public void setEl(Integer el) {
        switch (el) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                this.el = "input";
                break;
            default:
                this.el = "";
        }
    }

    private List<PropBean> prop = new ArrayList<>();
}
