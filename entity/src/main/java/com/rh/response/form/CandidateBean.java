package com.rh.response.form;

import lombok.Getter;
import lombok.Setter;

/** 多选配置参数
 * @Author: wangdao
 * @Date: 2021/1/24 18:49
 */
@Getter
@Setter
public class CandidateBean {
    private String label;
    private String value;
}
