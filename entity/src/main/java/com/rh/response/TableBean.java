package com.rh.response;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: wangdao
 * @Date: 2021/1/24 17:55
 */
@Getter
@Setter
public class TableBean {
    private boolean hideColumn;
    private boolean paging;
}
