package com.rh.result.module;

import com.rh.entity.module.RenHaiModule;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author qingmu
 * @since 2019-10-28
 */
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class RenHaiModuleResult extends RenHaiModule {

    private RenHaiModule pModule;

}
