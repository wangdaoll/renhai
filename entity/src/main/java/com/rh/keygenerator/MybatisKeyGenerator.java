package com.rh.keygenerator;

import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author 57556
 */
@Component
public class MybatisKeyGenerator implements IKeyGenerator {
    @Override
    public String executeSql(String incrementerName) {
        return UUID.randomUUID().toString();
    }
}

