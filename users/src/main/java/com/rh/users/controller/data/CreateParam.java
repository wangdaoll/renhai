package com.rh.users.controller.data;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rh.response.EntityBean;
import lombok.Getter;
import lombok.Setter;
import renhai.util.json.LongJsonDeserializer;
import renhai.util.json.LongJsonSerializer;

/** 添加修改参数
 * @Author: wangdao
 * @Date: 2021/2/19 17:52
 */
@Getter
@Setter
public class CreateParam extends EntityBean {
    /**
     * id id存在 修改 不存在新增
     */
    @JsonDeserialize(using = LongJsonDeserializer.class)
    @JsonSerialize(using = LongJsonSerializer.class)
    private Long id;
   /* *//**
     * 参数 {id:null,name:'sss'}
     *//*
    private ObjectNode fledNode;*/

}
