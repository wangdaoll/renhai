package com.rh.users.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.rh.entity.user.RenHaiUser;
import com.rh.entity.users.RenHaiTest;
import com.rh.users.service.RenHaiTestService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.Page;
import renhai.util.ResponseContext;
import renhai.util.RestTemplateUtil;
import renhai.util.id.IdUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
* <p>
    *   前端控制器
    * </p>
*
* @author wangdao
* @since 2020-05-21
*/
@RestController
@RequestMapping("/api/users/renHaiTest")
    public class RenHaiTestController {


    private final  RenHaiTestService renHaiTestService;

    private final RestTemplateUtil restTemplateUtil;

    @Autowired
    public RenHaiTestController(RenHaiTestService renHaiTestService, RestTemplateUtil restTemplateUtil) {
        this.renHaiTestService = renHaiTestService;
        this.restTemplateUtil = restTemplateUtil;
    }

    /**
    * 查询分页数据
    */
    @ApiOperation(value = "查询分页数据")
    @RequestMapping(value = "/getList")
    public ResponseContext<PageInfo<RenHaiTest>> getList(@RequestBody Page<RenHaiTest> page) {

        return new ResponseContext<>(renHaiTestService.list(page));
    }


    /**
    * 根据id查询
    */
    @ApiOperation(value = "根据id查询数据")
    @RequestMapping(value = "/getById")
    public ResponseContext<RenHaiTest> getById(@RequestBody RenHaiTest test){
        return new ResponseContext<>(renHaiTestService.getById(test.getId()));
    }

    /**
    * 新增
    */
    @ApiOperation(value = "新增/修改 数据")
    @RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
    public ResponseContext<Boolean> saveUpdate(@RequestBody RenHaiTest renHaiTest){
        RenHaiUser renHaiUser =  restTemplateUtil.getUser(RenHaiUser.class);
        if(renHaiTest.getId()==null){
            renHaiTest.setId(IdUtil.nextId());
            renHaiTest.setCreateId(renHaiUser.getUserId());
            renHaiTest.setCreateName(renHaiUser.getName());
            renHaiTest.setCreateTime(new Date());
        }
        renHaiTest.setUpdateName(renHaiUser.getName());
        renHaiTest.setUpdateId(renHaiUser.getUserId());
        renHaiTest.setUpdateTime(new Date());

        return new ResponseContext<>(renHaiTestService.saveOrUpdate(renHaiTest));
    }

    /**
    * 删除
    */
    @ApiOperation(value = "删除数据")
    @DeleteMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestBody String id){
        return new ResponseContext<>(renHaiTestService.removeById(id));
    }

    @ApiOperation(value = "删除数据")
    @RequestMapping(value = "/checkCode")
    public ResponseContext<Integer> checkCode(RenHaiTest renHaiTest){

        return new ResponseContext<>(renHaiTestService.count(
                new QueryWrapper<RenHaiTest>().lambda()
                        .eq(RenHaiTest::getCode,renHaiTest.getCode()).ne(renHaiTest.getId()!=null,RenHaiTest::getId,renHaiTest.getId())
        ));
    }
    @ApiOperation(value = "获取下拉框数据")
    @RequestMapping(value = "/getSelectMap")
    public ResponseContext<Map<String,String>> getSelectMap(){
        Map<String, String> map = new HashMap<>();
        map.put("1","name1");
        map.put("2","name2");
        map.put("3","name3");
        map.put("4","name4");
        return new ResponseContext<>(map);
    }


}
