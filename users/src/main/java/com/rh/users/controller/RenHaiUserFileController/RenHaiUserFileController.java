package com.rh.users.controller.RenHaiUserFileController;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rh.users.service.RenHaiUserFileService;
import com.rh.entity.user.RenHaiUserFile;
import renhai.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.ResponseContext;
import java.io.Serializable;

/**
* <p>
    *  前端控制器
    * </p>
*
* @author wangdao
* @since 2020-04-29
*/
    @RestController
 @RequestMapping("/api/users/user/file")
    public class RenHaiUserFileController {


    private final  RenHaiUserFileService renHaiUserFileService;

    @Autowired
    public RenHaiUserFileController(RenHaiUserFileService renHaiUserFileService) {
        this.renHaiUserFileService = renHaiUserFileService;
    }

    /**
    * 查询分页数据
    */
    @ApiOperation(value = "查询分页数据")
    @RequestMapping(value = "/getList")
    public ResponseContext<PageInfo<RenHaiUserFile>> getList(@RequestBody Page<RenHaiUserFile> page) {

        return new ResponseContext<>(renHaiUserFileService.list(page));
    }


    /**
    * 根据id查询
    */
    @ApiOperation(value = "根据id查询数据")
    @RequestMapping(value = "/getById")
    public ResponseContext<RenHaiUserFile> getById(@RequestParam("id") Serializable id){
        return new ResponseContext<>(renHaiUserFileService.getById(id));
    }

    /**
    * 新增
    */
    @ApiOperation(value = "新增/修改 数据")
    @RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
    public ResponseContext<Boolean> saveUpdate(@RequestBody RenHaiUserFile renHaiUserFile){
        return new ResponseContext<>(renHaiUserFileService.saveOrUpdate(renHaiUserFile));
    }

    /**
    * 删除
    */
    @ApiOperation(value = "删除数据")
    @RequestMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestParam("id") Serializable id){
        return new ResponseContext<>(renHaiUserFileService.removeById(id));
    }
}
