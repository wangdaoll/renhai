package com.rh.users.controller.RenHaiModuleController;

import com.github.pagehelper.PageInfo;
import com.rh.entity.module.RenHaiModule;
import com.rh.result.module.RenHaiModuleResult;
import com.rh.users.service.RenHaiModuleService;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;
import renhai.util.Constant;
import renhai.util.Page;
import renhai.util.ResponseContext;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
@RestController
@Api("模块")
@RequestMapping("/api/users/renHaiModule")
public class RenHaiModuleController {

    private final RenHaiModuleService service;

    @Autowired
    private ApplicationContext context;

    public RenHaiModuleController(RenHaiModuleService service) {
        this.service = service;
    }

    @PostMapping("saveUpdate")
    @ApiOperation("新增/修改")
    private ResponseContext<String> saveUpdate(@RequestBody RenHaiModule module) {
        if (module != null) {
            if (!StringUtils.isNotEmpty(module.getModuleId())) {
                service.save(module);
                return Constant.SAVE_S;
            } else {
                int state = service.update(module);
                //修改成功
                if (state == 1) {
                    return Constant.UPDATE_S;
                } else {
                    //修改失败
                    return Constant.UPDATE_E;
                }
            }
        }
        return Constant.ERROR;
    }


    @PostMapping("getList")
    @ApiOperation("获取集合")
    public ResponseContext<PageInfo<RenHaiModule>> getList(@RequestBody Page<RenHaiModule> page) {
        RenHaiModule module = new RenHaiModule();
        module.setModuleId(page.getParams().getModuleId());
        return new ResponseContext<>(service.list(page, module), 1);
    }

    /**
     * 查询模块树
     *
     * @return 模块树
     */
    @PostMapping("getListTree.action")
    @ApiOperation("查询模块树")
    public ResponseContext<RenHaiModule> getListTree() {
        return new ResponseContext<>(service.getListTree(null), 1);
    }

    @PostMapping("getMenu.action")
    @ApiOperation("查询菜单")
    public ResponseContext<RenHaiModule> getMenu() {
        return new ResponseContext<>(service.getMenu(), 1);
    }

    @DeleteMapping("delete")
    @ApiOperation("删除")
    public ResponseContext<String> delete(@RequestBody RenHaiModule module) {
        if (service.removeById(module.getModuleId())) {
            return Constant.DELETE_S;
        } else {
            return Constant.DELETE_E;
        }
    }
    @GetMapping("get")
    @ApiOperation("获取详情")
    public ResponseContext<RenHaiModuleResult> get(RenHaiModule module){

        return new ResponseContext<>(service.getById(module.getModuleId()));
    }

}

