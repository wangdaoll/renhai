package com.rh.users.controller;

import com.github.pagehelper.PageInfo;
import com.rh.response.EntityBean;
import com.rh.users.service.RenHaiEntityService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import renhai.util.Page;
import renhai.util.ResponseContext;

import java.util.Map;

/** 返回实体JSON
 * @Author: wangdao
 * @Date: 2021/1/24 15:32
 */
@RestController
@RequestMapping("api/users/ren/hai/entity")
public class EntityController {

    private final RenHaiEntityService entityService;

    public EntityController(RenHaiEntityService entityService) {
        this.entityService = entityService;
    }

    @GetMapping("/getEntity")
    public ResponseContext<EntityBean> getEntity(EntityBean entityBean){
        return new ResponseContext<>(entityService.getEntityBean(entityBean.getName()),0);
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping(value = "/getList")
    public ResponseContext<PageInfo<Object>> getList(@RequestBody Page<Map<String,Object>> page) {

        return new ResponseContext<>();
    }







}
