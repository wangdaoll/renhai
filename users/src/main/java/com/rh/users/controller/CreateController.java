package com.rh.users.controller;

import com.rh.users.controller.data.CreateParam;
import com.rh.users.service.CreateService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import renhai.util.ResponseContext;

/**
 * @Author: wangdao
 * @Date: 2021/2/19 17:13
 */
//@RestController
@RequestMapping("api/users/ren/hai/create")
public class CreateController {


    private final CreateService createService;

    public CreateController(CreateService createService) {
        this.createService = createService;
    }

    @PostMapping("/saveUpdate")
    public ResponseContext<Boolean> saveUpdate(@RequestBody CreateParam createParam){
        return createService.saveUpdate(createParam);
    }
}
