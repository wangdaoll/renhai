package com.rh.users.controller.RenHaiUserController;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class RenHaiUserControllerLogin {
    @NotBlank(message = "用户名不能为null")
    private String userName;
    @NotBlank(message = "密码不能为null")
    private String password;
}
