package com.rh.users.controller;


import com.github.pagehelper.PageInfo;
import com.rh.entity.users.RenHaiField;
import com.rh.users.service.RenHaiFieldService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renhai.util.Page;
import renhai.util.ResponseContext;

import java.io.Serializable;

/**
* <p>
    *   前端控制器
    * </p>
*
* @author wangdao
* @since 2020-11-21
*/
    @RestController
@RequestMapping("/api/users/renHaiField")
    public class RenHaiFieldController {


    private final  RenHaiFieldService renHaiFieldService;

    @Autowired
    public RenHaiFieldController(RenHaiFieldService renHaiFieldService) {
        this.renHaiFieldService = renHaiFieldService;
    }

    /**
    * 查询分页数据
    */
    @ApiOperation(value = "查询分页数据")
    @PostMapping(value = "/getList")
    public ResponseContext<PageInfo<RenHaiField>> getList(@RequestBody Page<RenHaiField> page) {

        return new ResponseContext<>(renHaiFieldService.list(page));
    }
    /**
    * 根据id查询
    */
    @ApiOperation(value = "根据id查询数据")
    @GetMapping(value = "/getById")
    public ResponseContext<RenHaiField> getById(@RequestParam("id") Serializable id){
        return new ResponseContext<>(renHaiFieldService.getById(id));
    }

    /**
    * 新增
    */
    @ApiOperation(value = "新增/修改 数据")
    @PostMapping(value = "/saveUpdate")
    public ResponseContext<RenHaiField> saveUpdate(@RequestBody RenHaiField renHaiField){
        boolean b = renHaiFieldService.saveOrUpdate(renHaiField);
        if(b){
            return new ResponseContext<>(renHaiField,1);
        }
        return new ResponseContext<>(renHaiField,0);
    }

    /**
    * 删除
    */
    @ApiOperation(value = "删除数据")
    @DeleteMapping(value = "/del")
    public ResponseContext<Boolean> delete(@RequestBody RenHaiField renHaiField){
        return new ResponseContext<>(renHaiFieldService.removeById(renHaiField.getId()));
    }
}
