package com.rh.users.controller.RenHaiUserController;

import com.github.pagehelper.PageInfo;
import com.rh.entity.user.RenHaiUser;
import com.rh.users.service.RenHaiUserService;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import renhai.util.Constant;
import renhai.util.Page;
import renhai.util.ResponseContext;
import renhai.util.RestTemplateUtil;
import renhai.util.redis.RedisUrl;

import javax.validation.Valid;

/**
 * @author 57556
 */
@Api("用户")
@RestController
@RequestMapping("/api/users/user")
public class RenHaiUserController {
    private final RestTemplateUtil restTemplateUtil;

    private final RenHaiUserService userService;

    public RenHaiUserController(RestTemplateUtil restTemplateUtil, RenHaiUserService userService) {
        this.restTemplateUtil = restTemplateUtil;
        this.userService = userService;
    }

    @ApiOperation("用户登陆")
    @PostMapping("/login.action")
    public ResponseContext<String> login(@RequestBody @ApiParam(hidden = true) @Valid RenHaiUserControllerLogin login) {
        RenHaiUser user = new RenHaiUser();
        user.setUserName(login.getUserName());
        user.setPassword(login.getPassword());
        RenHaiUser users = userService.login(user);
        ResponseContext<String> responseContext = new ResponseContext<String>();
        if (users != null && users.getUserId() != null) {
            //获取token
            ResponseContext token = restTemplateUtil.postForObject(RedisUrl.SAVE_TOKEN, users);
            if (token != null && token.getEntity() != null) {
                responseContext.setState(1);
                responseContext.setEntity(token.getEntity().toString());
            } else {
                responseContext.setState(0);
                responseContext.setMsg("网络异常！");
            }
        } else {
            responseContext.setState(0);
            responseContext.setMsg("用户名或密码错误！");
        }
        return responseContext;
    }

    ;

    @ApiOperation( "新增/修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user", value = "用户信息", required = true)
    })
    @PostMapping("/saveUpdate")
    public ResponseContext<String> saveUpdate(@RequestBody @ApiParam(hidden = true) RenHaiUser user) {
        if (!StringUtils.isNotBlank(user.getUserId())) {
            userService.save(user, user.getRoleIds());
            return new ResponseContext<>(user.getUserId(), "添加成功");
        } else {
            userService.update(user, user.getRoleIds());
            return new ResponseContext<>(user.getUserId(), "修改成功");
        }
    }

    @ApiOperation("获取用户详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true)
    })
    @PostMapping("/getDetails")
    public ResponseContext<RenHaiUser> getDetails(@RequestBody @ApiParam(hidden = true) RenHaiUser user) {

        return new ResponseContext<>(userService.getDetails(user.getUserId()), "用户信息");
    }

    @ApiOperation( "查询用户列表"  )
    @PostMapping("/getUserList")
    public ResponseContext<PageInfo> getUserList(@RequestBody @ApiParam(hidden = true) Page<RenHaiUser> page) {

        return new ResponseContext<>(userService.list(page, page.getParams()));
    }

    @ApiOperation("删除用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", required = true)
    })
    @DeleteMapping("/delete")
    public ResponseContext<String> delete(@RequestBody @ApiParam(hidden = true) RenHaiUser user) {
        if (userService.deleteById(user.getUserId())) {
            return Constant.DELETE_S;
        }
        return Constant.DELETE_E;
    }

    @ApiOperation("检查用户名是否重复")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名", required = true),
            @ApiImplicitParam(name = "userId", value = "用户id", required = true)
    })
    @PostMapping("/getUserNameCount")
    public ResponseContext<Integer> getUserNameCount(@RequestBody @ApiParam(hidden = true) RenHaiUser user) {
        return new ResponseContext<>(userService.getUserNameCount(user.getUserName(), user.getUserId()), "检查用户名是否重复");
    }

}
