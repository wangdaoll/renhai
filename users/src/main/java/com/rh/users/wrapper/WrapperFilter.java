package com.rh.users.wrapper;

/**
 * @Author: wangdao
 * @Date: 2021/2/1 22:12
 */
public class WrapperFilter {
    /**
     * 字段名称
     */
    private String name;
    /**
     * 条件
     */
    private String condition;
    /**
     * value 值
     */
    private Object value;
    /**
     * 实体名称
     */
    private String entityName;

}
