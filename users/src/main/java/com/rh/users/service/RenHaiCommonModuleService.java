package com.rh.users.service;

import com.rh.entity.users.RenHaiCommonModule;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;

/**
 * <p>
 * 公共组件  服务类
 * </p>
 *
 * @author wangdao
 * @since 2020-07-22
 */
public interface RenHaiCommonModuleService extends IService<RenHaiCommonModule> {

   PageInfo<RenHaiCommonModule> list(Page<RenHaiCommonModule> page);

}
