package com.rh.users.service.command;

import java.util.ArrayList;
import java.util.List;

/** 命令上下文
 * @Author: wangdao
 * @Date: 2021/2/19 17:23
 */
public class CommandContext {
    private static final ThreadLocal<CommandContext> commandContext = new ThreadLocal<>();
    public static CommandContext  getContext(){
        CommandContext ctx = commandContext.get();
        if (ctx != null) {
            return ctx;
        }
        ctx = new CommandContext();
        commandContext.set(ctx);
        return ctx;
    }
    /**
     * 等待执行的命令
     */
    private List<Command> awaitExecutesCommands = new ArrayList<>();
    /**
     * 添加命令
     * @param command
     * @return
     */
    public CommandContext addCommand(Command command) {
        awaitExecutesCommands.add(command);
        return this;
    }
    /**
     * 删除命令
     * @param command
     * @return
     */
    public CommandContext removeCommand(Command command){
        awaitExecutesCommands.remove(command);
        return this;
    }
    public CommandContext removeCommand(Integer command){
        awaitExecutesCommands.remove(command);
        return this;
    }

    /**
     * 执行命令
     */
    public void execute(){
        if(awaitExecutesCommands.isEmpty()){
           return;
        }
        final Object[] object = {null};
        awaitExecutesCommands.forEach(x->{
            object[0] = x.execute(object[0]);
        });
    }
}
