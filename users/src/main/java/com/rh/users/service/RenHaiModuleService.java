package com.rh.users.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.rh.entity.module.RenHaiModule;
import com.rh.result.module.RenHaiModuleResult;
import renhai.util.Page;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author qingmu
 * @since 2019-10-28
 */
public interface RenHaiModuleService extends IService<RenHaiModule> {
    /**
     * 修改模块
     *
     * @param module 模块
     * @return 返回受影响的行数
     */
    int update(RenHaiModule module);

    /**
     * 获取模块菜单树
     *
     * @param module 模块
     * @return 模块树
     */
    List<RenHaiModule> getListTree(RenHaiModule module);

    /**
     * 获取菜单
     * @return
     */
    List<RenHaiModule> getMenu();

    /**
     * @param module 查询参数
     * @return 分页查询
     */
    PageInfo<RenHaiModule> list(Page page, RenHaiModule module);

    /**
     * 获取模块详情
     * @param id
     * @return
     */
    RenHaiModuleResult getById(String id);
}
