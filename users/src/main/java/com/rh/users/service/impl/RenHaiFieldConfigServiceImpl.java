package com.rh.users.service.impl;

import com.rh.entity.users.RenHaiFieldConfig;
import com.rh.users.mapper.RenHaiFieldConfigMapper;
import com.rh.users.service.RenHaiFieldConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@Service
public class RenHaiFieldConfigServiceImpl extends ServiceImpl<RenHaiFieldConfigMapper, RenHaiFieldConfig> implements RenHaiFieldConfigService {

   private final RenHaiFieldConfigMapper renHaiFieldConfigMapper;

   private final RestTemplateUtil restTemplateUtil;

   public  RenHaiFieldConfigServiceImpl(RenHaiFieldConfigMapper renHaiFieldConfigMapper, RestTemplateUtil restTemplateUtil) {
     this.renHaiFieldConfigMapper = renHaiFieldConfigMapper;
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<RenHaiFieldConfig> list(Page<RenHaiFieldConfig> page) {
       RenHaiFieldConfig params = page.getParams();
       QueryWrapper<RenHaiFieldConfig> queryWrapper = new QueryWrapper<>();
       queryWrapper.lambda()
               .eq(params.getFieldId()!=null,RenHaiFieldConfig::getFieldId,params.getFieldId())
               .isNull(true,RenHaiFieldConfig::getParentId);
     PageHelper.startPage(page);
     return new PageInfo<>(renHaiFieldConfigMapper.selectList(queryWrapper));
   }
}
