package com.rh.users.service.impl;

import com.rh.entity.users.RenHaiCommonModule;
import com.rh.users.mapper.RenHaiCommonModuleMapper;
import com.rh.users.service.RenHaiCommonModuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

/**
 * <p>
 * 公共组件  服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-07-22
 */
@Service
public class RenHaiCommonModuleServiceImpl extends ServiceImpl<RenHaiCommonModuleMapper, RenHaiCommonModule> implements RenHaiCommonModuleService {

   private final RenHaiCommonModuleMapper renHaiCommonModuleMapper;

   private final RestTemplateUtil restTemplateUtil;

   public  RenHaiCommonModuleServiceImpl(RenHaiCommonModuleMapper renHaiCommonModuleMapper, RestTemplateUtil restTemplateUtil) {
     this.renHaiCommonModuleMapper = renHaiCommonModuleMapper;
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<RenHaiCommonModule> list(Page<RenHaiCommonModule> page) {
     QueryWrapper<RenHaiCommonModule> queryWrapper = new QueryWrapper<>();
     PageHelper.startPage(page);
     return new PageInfo<>(renHaiCommonModuleMapper.selectList(queryWrapper));
   }
}
