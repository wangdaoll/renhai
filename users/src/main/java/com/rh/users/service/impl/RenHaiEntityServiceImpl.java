package com.rh.users.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rh.entity.users.RenHaiEntity;
import com.rh.entity.users.RenHaiField;
import com.rh.entity.users.RenHaiFieldConfig;
import com.rh.response.EntityBean;
import com.rh.response.FieldBean;
import com.rh.users.mapper.RenHaiEntityMapper;
import com.rh.users.service.RenHaiEntityService;
import com.rh.users.service.RenHaiFieldConfigService;
import com.rh.users.service.RenHaiFieldService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.stereotype.Service;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;
import renhai.util.bean.ClassTools;
import renhai.util.bean.FieldUtil;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
@Service
public class RenHaiEntityServiceImpl extends ServiceImpl<RenHaiEntityMapper, RenHaiEntity> implements RenHaiEntityService {

    private final RenHaiEntityMapper renHaiEntityMapper;

    private final RenHaiFieldService fieldService;

    private final RenHaiFieldConfigService fieldConfigService;

    private final RestTemplateUtil restTemplateUtil;

    public RenHaiEntityServiceImpl(RenHaiEntityMapper renHaiEntityMapper, RenHaiFieldService fieldService, RenHaiFieldConfigService fieldConfigService, RestTemplateUtil restTemplateUtil) {
        this.renHaiEntityMapper = renHaiEntityMapper;
        this.fieldService = fieldService;
        this.fieldConfigService = fieldConfigService;
        this.restTemplateUtil = restTemplateUtil;
    }

    @Override
    public PageInfo<RenHaiEntity> list(Page<RenHaiEntity> page) {
        QueryWrapper<RenHaiEntity> queryWrapper = new QueryWrapper<>();
        PageHelper.startPage(page);
        return new PageInfo<>(renHaiEntityMapper.selectList(queryWrapper));
    }

    @Override
    public EntityBean getEntityBean(String name) {
        EntityBean entityBean = new EntityBean();
        QueryWrapper<RenHaiEntity> entityWrapper = new QueryWrapper<>();
        entityWrapper.lambda().eq(true, RenHaiEntity::getName, name);
        RenHaiEntity entity = renHaiEntityMapper.selectOne(entityWrapper);
        entityBean.setTitle(entity.getTitle());
        entityBean.setName(entity.getName());
        List<RenHaiField> fields = fieldService.list(new QueryWrapper<RenHaiField>().lambda().eq(true, RenHaiField::getEntityId, entity.getId()).orderByDesc(RenHaiField::getCreateTime));
        List<RenHaiFieldConfig> fieldConfigs = fieldConfigService.list(new QueryWrapper<RenHaiFieldConfig>().lambda().eq(true, RenHaiFieldConfig::getEntityId, entity.getId()).orderByDesc(RenHaiFieldConfig::getCreateTime));
        fields.forEach(x -> {
            List<RenHaiFieldConfig> configs = fieldConfigs.stream().filter(v -> x.getId().equals(v.getFieldId())).collect(Collectors.toList());
            FieldBean fieldBean = new FieldBean();
            fieldBean.setName(x.getName());
            fieldBean.setTitle(x.getTitle());
            if (x.getType() == null) {
                x.setType(1);
            }
            fieldBean.getFormParam().getParams().setEl(x.getType());
            entityBean.getFields().add(fieldBean);
        });
        return entityBean;
    }

    /**
     * 初始化bean
     *
     * @return
     */
    @Override
    public Boolean initAllBeans() {

        Set<Class<?>> classes = ClassTools.getClasses("com.rh.entity");


        for (Class<?> clazz : classes) {
            ApiModel apiModel = clazz.getAnnotation(ApiModel.class);
            QueryWrapper<RenHaiEntity> entityWrapper = new QueryWrapper<>();
            entityWrapper.lambda().eq(true, RenHaiEntity::getName, clazz.getSimpleName());
            RenHaiEntity entity = renHaiEntityMapper.selectOne(entityWrapper);
            if(entity == null){
                entity = new RenHaiEntity();
            }
            entity.setName( clazz.getSimpleName());
            if(apiModel!=null){
                entity.setTitle(apiModel.value());
            }
            saveOrUpdate(entity);
            List<Field> allField = FieldUtil.getAllField(clazz);
            for (Field field : allField) {
                ApiModelProperty apiModelProperty = field.getAnnotation(ApiModelProperty.class);
                QueryWrapper<RenHaiField> fieldWrapper = new QueryWrapper<>();
                fieldWrapper.lambda().eq(true, RenHaiField::getName, clazz.getSimpleName())
                .eq(true,RenHaiField::getEntityId,entity.getId());
                RenHaiField renHaiField = fieldService.getOne(fieldWrapper);
                if(renHaiField == null){
                    renHaiField = new RenHaiField();
                }
                renHaiField.setName(field.getName());
                if(apiModelProperty!=null){
                    renHaiField.setTitle(!apiModelProperty.name().equals("") ? apiModelProperty.name() :apiModelProperty.value());
                }
                renHaiField.setType(FieldUtil.getType(field.getType()));
                renHaiField.setFieldEntityId(entity.getId());
                fieldService.saveOrUpdate(renHaiField);
            }
        }
        return true;
    }
}
