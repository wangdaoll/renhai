package com.rh.users.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.rh.entity.users.RenHaiEntity;
import com.rh.response.EntityBean;
import renhai.util.Page;

/**
 * <p>
 *   服务类
 * </p>
 *
 * @author wangdao
 * @since 2020-11-21
 */
public interface RenHaiEntityService extends IService<RenHaiEntity> {

   PageInfo<RenHaiEntity> list(Page<RenHaiEntity> page);

   EntityBean getEntityBean(String name);

   Boolean initAllBeans();

}
