package com.rh.users.service.command;

/** 命令
 * @Author: wangdao
 * @Date: 2021/2/19 17:19
 */
public interface Command<T> {
    T execute(Object object);
}
