package com.rh.users.service;

import com.rh.users.controller.data.CreateParam;
import renhai.util.ResponseContext;

/**
 * @Author: wangdao
 * @Date: 2021/2/19 17:14
 */
public interface CreateService {
    /**
     * 添加/修改
     * @param createParam
     * @return
     */
    ResponseContext<Boolean> saveUpdate(CreateParam createParam);
}
