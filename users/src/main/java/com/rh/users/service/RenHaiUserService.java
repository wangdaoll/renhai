package com.rh.users.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.rh.entity.user.RenHaiUser;
import renhai.util.Page;

/**
 * @author 57556
 */
public interface RenHaiUserService extends IService<RenHaiUser> {
    /**
     * 登陆
     *
     * @param user
     * @return 返回用户信息
     */
    RenHaiUser login(RenHaiUser user);

    /**
     * 查询用户列表
     *
     * @param page
     * @param user
     * @return
     */
    PageInfo list(Page<RenHaiUser> page, RenHaiUser user);

    boolean save(RenHaiUser user, String[] roleIds);

    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    boolean deleteById(String id);

    boolean update(RenHaiUser user, String[] roleIds);

    /**
     * 查询用户详情
     *
     * @param id
     * @return
     */
    RenHaiUser getDetails(String id);

    /**
     * 检查用户名是否重复
     *
     * @param userName 用户名
     * @param userId   用户id
     * @return
     */
    Integer getUserNameCount(String userName, String userId);

    /**
     * 修改用户密码
     *
     * @param userId   用户id
     * @param password 密码
     * @return
     */
    boolean updatePassword(String userId, String password, String salt);


}
