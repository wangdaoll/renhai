package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import renhai.util.Page;
import renhai.util.RestTemplateUtil;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

   private final ${table.mapperName} ${table.mapperName?uncap_first};

   private final RestTemplateUtil restTemplateUtil;

   public  ${table.serviceImplName}(${table.mapperName} ${table.mapperName?uncap_first}, RestTemplateUtil restTemplateUtil) {
     this.${table.mapperName?uncap_first} = ${table.mapperName?uncap_first};
     this.restTemplateUtil = restTemplateUtil;
   }

   @Override
   public PageInfo<${entity}> list(Page<${entity}> page) {
     QueryWrapper<${entity}> queryWrapper = new QueryWrapper<>();
     PageHelper.startPage(page);
     return new PageInfo<>(${table.mapperName?uncap_first}.selectList(queryWrapper));
   }
}
</#if>
