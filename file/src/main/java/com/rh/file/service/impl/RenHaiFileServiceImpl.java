package com.rh.file.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rh.entity.file.RenHaiFile;
import com.rh.entity.file.RenHaiFileDetails;
import com.rh.file.controller.FileParam;
import com.rh.file.mapper.RenHaiFileMapper;
import com.rh.file.service.RenHaiFileDetailsService;
import com.rh.file.service.RenHaiFileService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import renhai.util.DataUtil;

import java.util.List;

@Service
public class RenHaiFileServiceImpl extends ServiceImpl<RenHaiFileMapper, RenHaiFile> implements RenHaiFileService {


    private final RenHaiFileDetailsService fileDetailsService;
    private final RenHaiFileMapper renHaiFileMapper;
    @Value("${upload.path}")
    private String uploadPath;

    public RenHaiFileServiceImpl(RenHaiFileDetailsService fileDetailsService, RenHaiFileMapper renHaiFileMapper) {
        this.fileDetailsService = fileDetailsService;
        this.renHaiFileMapper = renHaiFileMapper;
    }

    @Override
    public RenHaiFile checkUploading(FileParam param) {
        /**
         * 检查此文件是否上传
         */
        RenHaiFile file = this.getByMD5(param);
        if (file != null) {
            return file;
        }
        /**
         *
         * 添加一条文件记录
         */
        return insert(param);
    }

    @Override
    public RenHaiFile insert(FileParam param) {
        RenHaiFile file = new RenHaiFile();
        //md5
        file.setMd5(param.getIdentifier());
        //未完成
        file.setComplete(0);
        //分块大小
        file.setChunkSize(param.getChunkSize());
        //分成的块数
        file.setTotalChunks(param.getTotalChunks());
        //文件总大小
        file.setTotalSize(param.getTotalSize());
        //地址
        String url = DataUtil.getData("/");
        file.setUrl(url);
        this.save(file);
        return file;
    }

    @Override
    public RenHaiFile getByMD5(FileParam param) {
        QueryWrapper<RenHaiFile> queryWrapper = new QueryWrapper<>();
        //uuid
        queryWrapper.lambda().eq(StringUtils.isNotBlank(param.getTaskId()), RenHaiFile::getMd5, param.getTaskId());
        //md5
        queryWrapper.lambda().eq(true, RenHaiFile::getMd5, param.getIdentifier());
        //文件分块大小
        queryWrapper.lambda().eq(true, RenHaiFile::getChunkSize, param.getChunkSize());
        //文件被分成的块数
        queryWrapper.lambda().eq(true, RenHaiFile::getTotalChunks, param.getTotalChunks());
        RenHaiFile file = null;
        List<RenHaiFile> list = renHaiFileMapper.selectList(queryWrapper);
        if (list != null && !list.isEmpty()) {
            file = list.get(0);
        }
        //检查文件是否上传完成 0 未完成 1 已完成
        if (file != null && file.getComplete() == 0) {
            QueryWrapper<RenHaiFileDetails> queryFileDetails = new QueryWrapper<>();
            queryFileDetails.lambda().eq(true, RenHaiFileDetails::getRenHaiFile, file.getFileId());
            file.setFileDetailsList(fileDetailsService.list(queryFileDetails));
        }
        return file;
    }

    @Override
    public void updateComplete(String fileId,String url) {
        UpdateWrapper<RenHaiFile> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda()
                .set(true, RenHaiFile::getComplete, 1)
                .set(true, RenHaiFile::getUrl,url)
                .eq(RenHaiFile::getFileId, fileId);
        if (renHaiFileMapper.update(null, updateWrapper) >= 1) {
            fileDetailsService.deleteById(fileId);
        }
    }


}
