package com.rh.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rh.entity.file.RenHaiFileDetails;
import com.rh.file.controller.FileParam;

public interface RenHaiFileDetailsService extends IService<RenHaiFileDetails> {
    /***
     *
     * @param param 参数
     * @return true
     */
    boolean deleteById(String param);

    RenHaiFileDetails insert(FileParam param);



}
