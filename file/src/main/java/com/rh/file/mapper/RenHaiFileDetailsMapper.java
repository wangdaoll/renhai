package com.rh.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rh.entity.file.RenHaiFileDetails;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RenHaiFileDetailsMapper extends BaseMapper<RenHaiFileDetails> {
    Integer deleteById(String fileId);
}
