package com.rh.activiti.config;

import org.activiti.spring.SpringProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * @author 57556
 *
 * Activiti  配置
 *
 */
@Configurable
public class ActivitiConfiguration {
    /**
     * spring  集成 activiti
     * @param dataSource 数据源
     * @param transactionManager spring 事务
     * @return
     */
    @Bean
    public SpringProcessEngineConfiguration springProcessEngineConfiguration(DataSource dataSource, DataSourceTransactionManager transactionManager) {
        SpringProcessEngineConfiguration springProcessEngineConfiguration = new SpringProcessEngineConfiguration();
        springProcessEngineConfiguration.setDatabaseSchemaUpdate("true");
        //数据源
        springProcessEngineConfiguration.setDataSource(dataSource);
        //spring 事务
        springProcessEngineConfiguration.setTransactionManager(transactionManager);
        //初始化引擎
        springProcessEngineConfiguration.buildProcessEngine();

        return springProcessEngineConfiguration;
    }

}
